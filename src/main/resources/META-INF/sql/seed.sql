insert ignore into sec_group(groupname, groupdesc) values('Players','This group contains Players.');
insert ignore into sec_group(groupname, groupdesc) values('Parents','This group contains Parents.');

insert ignore into sec_user(username, password, enabled) values('player1',SHA2('player1', 256), true); 
insert ignore into sec_user(username, password, enabled) values('player2',SHA2('player2', 256), true); 
insert ignore into sec_user(username, password, enabled) values('parent1',SHA2('parent1', 256), true); 
insert ignore into sec_user(username, password, enabled) values('parent2',SHA2('parent2', 256), true);
insert ignore into sec_user(username, password, enabled) values('damarjian', SHA2('damarjian', 256), true);  


insert ignore into sec_user_groups(username, groupname) values('player1','Players');
insert ignore into sec_user_groups(username, groupname) values('player2','Players');
insert ignore into sec_user_groups(username, groupname) values('damarjian','Players');

insert ignore into sec_user_groups(username, groupname) values('parent1','Parents');
insert ignore into sec_user_groups(username, groupname) values('parent2','Parents');
insert ignore into sec_user_groups(username, groupname) values('damarjian','Parents');


insert into bank(id, dateAccountCreated, name) values (1,'1973-02-01','Centrust Bank');
insert into bank(id, dateAccountCreated, name) values (2,'1969-07-01','Chase');

insert into parent(id,  name, username) values (1, 'Alex Damarjian','damarjian');


insert into player(id, birthday, email, name, bank_id, username) values (1, '1980-08-01', 'fran man@iit.edu', 'Bo', 1,'player1');
insert into player(id, birthday, email, name, bank_id, username) values (2, '1980-08-01', 'Bob@iit.edu', 'Alex', 2,'player2');
insert into player(id, birthday, email, name, bank_id, username) values (3, '1979-09-18', 'adamarjian@colum.edu', 'Alex Damarjian', 2,'damarjian');



 insert into videogame(id, datecreated, name) values (1,'1973-02-01','Red Dead Redemption');
insert into videogame(id, datecreated, name) values (2,'1973-02-01','Red Dead Redemption 02');
insert into videogame(id, datecreated, name) values (3,'1973-02-01','Redemption 02');
insert into videogame(id, datecreated, name) values (4,'1973-02-01','Dead Redemption 02');
insert into videogame(id, datecreated, name) values (5,'1973-02-01','Red ');

insert into players_videogames(player_id, game_id) values (1,1);
insert into players_videogames(player_id, game_id) values (2,3);
insert into players_videogames(player_id, game_id) values (3,4);
insert into players_videogames(player_id, game_id) values (3,5);