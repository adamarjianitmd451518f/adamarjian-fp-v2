/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.iit.sat.itmd4515.adamarjian.service;
import edu.iit.sat.itmd4515.adamarjian.domain.Parent;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author EXO
 */
@Named
@Stateless
public class ParentService extends AbstractService<Parent> {

    public ParentService() {
        super(Parent.class);
    }
    
        @Override
    public List<Parent> findAll() {
        return getEntityManager().createNamedQuery("Parent.findAll", Parent.class).getResultList();
    }
    
    public Parent findByUserName(String username) {
        return getEntityManager().createNamedQuery("Parent.findByUsername", Parent.class).setParameter("username", username).getSingleResult();
    }
    
}
