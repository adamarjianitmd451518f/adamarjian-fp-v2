/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author EXO
 */
@Entity
@Table(name = "bank")
//@NamedQueries({
//    @NamedQuery(
//            name = "Bank.findByName",
//            query = "select b from Bank b where b.name = :name"),
//    @NamedQuery(
//            name = "Bank.findAll",
//            query = "select b from Bank b")
//})
public class Bank extends AbstractIdentifiedEntity {

    @Column(name = "name", unique = true)
    private String name;
    @Column(name = "dateAccountCreated")
    @Temporal(TemporalType.DATE)
    private Date dateAccountCreated;
    
    @OneToMany(mappedBy = "bank")
    private List<Player> players;

    public Bank(String name, Date dateAccountCreated) {
        this.name = name;
        this.dateAccountCreated = dateAccountCreated;
    }

    public Bank() {
    }
    
    

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the value of dateAccountCreated
     *
     * @return the value of dateAccountCreated
     */
    public Date getDateAccountCreated() {
        return dateAccountCreated;
    }

    /**
     * Set the value of dateAccountCreated
     *
     * @param dateAccountCreated new value of dateAccountCreated
     */
    public void setDateAccountCreated(Date dateAccountCreated) {
        this.dateAccountCreated = dateAccountCreated;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "Bank{" + "name=" + name + ", dateAccountCreated=" + dateAccountCreated + ", players=" + players + '}';
    }

}
