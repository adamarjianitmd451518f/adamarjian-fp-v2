/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.service;

import edu.iit.sat.itmd4515.adamarjian.domain.Bank;
import edu.iit.sat.itmd4515.adamarjian.domain.VideoGame;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author EXO
 */
@Stateless
public class VideoGameService extends AbstractService<VideoGame> {

    public VideoGameService() {
        super(VideoGame.class);
    }
    
    @Override
    public List<VideoGame> findAll() {
        return getEntityManager().createNamedQuery("VideoGame.findAll", VideoGame.class).getResultList();
    }

    
}
