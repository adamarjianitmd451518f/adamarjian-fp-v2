
package edu.iit.sat.itmd4515.adamarjian.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public abstract class AbstractService<T> {

    @PersistenceContext(unitName = "itmd4515PU")
    private EntityManager em;

    private final Class<T> entityClass;

    public AbstractService(Class entityClass) {
        this.entityClass = entityClass;
    }

    protected EntityManager getEntityManager() {
        return em;
    }

    public void create(T entity) {
        em.persist(entity);
    }

    public T find(Object id) {
        return em.find(entityClass, id);
    }


    public abstract List<T> findAll();
    
    public void update(T entity) {
        em.merge(entity);
    }

    public void remove(T entity) {
        em.remove(em.merge(entity));
    }
}
