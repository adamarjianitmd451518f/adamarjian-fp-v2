/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.service;

import edu.iit.sat.itmd4515.adamarjian.domain.Bank;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author EXO
 */
@Named
@Stateless
public class BankService extends AbstractService<Bank> {


    public BankService() {
        super(Bank.class);
    }
    
    
    @Override
    public List<Bank> findAll() {
        return getEntityManager().createNamedQuery("Bank.findAll", Bank.class).getResultList();
    }

}
