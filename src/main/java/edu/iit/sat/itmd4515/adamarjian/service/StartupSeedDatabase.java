
package edu.iit.sat.itmd4515.adamarjian.service;


import edu.iit.sat.itmd4515.adamarjian.security.Group;
import edu.iit.sat.itmd4515.adamarjian.security.User;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;


@Startup
@Singleton
public class StartupSeedDatabase {
    
    @EJB private UserService userService;
    @EJB private GroupService groupService;
    
    @PostConstruct
    private void seedDatabase(){
        Group adminGroup = new Group("ADMINS", "Users in this group are administrators of the system.");
        User admin = new User("admin", "admin", true);
        admin.addGroup(adminGroup);
        
        groupService.create(adminGroup);
        userService.create(admin);
    }
    
}
