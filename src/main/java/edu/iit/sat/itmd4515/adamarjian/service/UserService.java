
package edu.iit.sat.itmd4515.adamarjian.service;

import edu.iit.sat.itmd4515.adamarjian.security.User;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author sas691
 */
@Stateless
public class UserService extends AbstractService<User> {

    public UserService() {
        super(User.class);
    }

    @Override
    public List<User> findAll() {
        return getEntityManager().createNamedQuery("User.findAll", User.class).getResultList();
    }
}
