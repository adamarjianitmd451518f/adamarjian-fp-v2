
package edu.iit.sat.itmd4515.adamarjian.domain;

import edu.iit.sat.itmd4515.adamarjian.security.User;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author EXO
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "Parent.findByName",
            query = "select p from Parent p where p.name = :name")
    ,
    @NamedQuery(
            name = "Parent.findAll",
            query = "select p from Parent p"),
    @NamedQuery(
            name = "Parent.findByUsername",
            query = "select p from Parent p where p.user.userName = :username")    
})
public class Parent extends AbstractIdentifiedEntity {
    
    private String name;

    @OneToOne
    @JoinColumn(name = "USERNAME")
    private User user;

    public Parent() {
    }

    
    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Parent{" + "name=" + name + '}';
    }
    
}
