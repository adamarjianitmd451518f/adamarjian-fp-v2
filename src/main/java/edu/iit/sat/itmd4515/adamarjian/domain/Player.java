package edu.iit.sat.itmd4515.adamarjian.domain;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import edu.iit.sat.itmd4515.adamarjian.security.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 * @author EXO
 */
@Entity
@Table(name = "player")
@NamedQueries({
    @NamedQuery(
            name = "Player.findByName",
            query = "select p from Player p where p.name = :name"),
    @NamedQuery(
            name = "Player.findAll",
            query = "select p from Player p"),
        @NamedQuery(
            name = "Player.findByUsername",
            query = "select p from Player p where p.user.userName = :username")
})
public class Player extends AbstractIdentifiedEntity {
    
    @ManyToOne
    @JoinColumn(name = "Bank_ID")
    private Bank bank;
    

    @NotBlank
    private String name;

    @Email
    @NotBlank
    @Column(name = "email")
    private String email;
    
    @OneToOne
    @JoinColumn(name = "USERNAME")
    private User user;

    @Column(name = "birthday")
    @Temporal(TemporalType.DATE)
    private Date birthday;

    
    
    @ManyToMany(mappedBy = "players")
    private List<VideoGame> videogames= new ArrayList<>();
    
    

    public Player() {
    }



    public Player(String name, String email, Date birthday) {
        this.name = name;
        this.email = email;
        this.birthday = birthday;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

  

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
    
    
    public void removeBank(){
        this.bank.getPlayers().remove(this);
        this.bank = null;
    }
    
    public Bank getBank() {
        return bank;
    }
    public void setBank(Bank bank) {
        this.bank = bank;
        
         if (!this.bank.getPlayers().contains(this)) {
            this.bank.getPlayers().add(this);
        }
        
        
    }
    
    
    

    
    public List<VideoGame> getVideogames() {
        return videogames;
    }
    public void setVideogames(List<VideoGame> videogames) {
        this.videogames = videogames;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Player{" + "bank=" + bank + ", name=" + name + ", email=" + email + ", user=" + user + ", birthday=" + birthday + ", videogames=" + videogames + '}';
    }

}
