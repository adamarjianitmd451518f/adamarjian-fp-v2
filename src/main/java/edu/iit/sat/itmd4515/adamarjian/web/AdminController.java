
package edu.iit.sat.itmd4515.adamarjian.web;
import edu.iit.sat.itmd4515.adamarjian.domain.Player;
import edu.iit.sat.itmd4515.adamarjian.domain.VideoGame;
import edu.iit.sat.itmd4515.adamarjian.service.PlayerService;
import edu.iit.sat.itmd4515.adamarjian.service.PlayerService;
import edu.iit.sat.itmd4515.adamarjian.web.AbstractController;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author EXO
 */
@Named
@RequestScoped
public class AdminController extends AbstractController {

    private static final Logger LOG = Logger.getLogger(AdminController.class.getName());
    private Player player;

    @EJB
    PlayerService playerSvc;
    private List<Player> players;
    
    @Override
    @PostConstruct
    protected void postConstruct() {
        players = playerSvc.findAll();
        player = new Player();
        
        LOG.info("Inside AdminController.postConstruct()");
        super.postConstruct();
    }

    // action methods
    public String doCreatePlayer() {
        LOG.info("Inside AdminController.doCreateDj() with " + player.toString());
        return "/admin/createPlayer";
    }

    public String doViewPlayer(Player player) {
        this.player = player;
        LOG.info("Inside AdminController.doViewPlayer() with " + player.toString());
        return "/admin/viewPlayer";
    }

    public String doEditPlayer(Player player) {
        this.player = player;
        LOG.info("Inside AdminController.doEditPlayer() with " + player.toString());
        return "/admin/editPlayer";
    }

    
    public String doDeletePlayer(Player player) {
        LOG.info("Inside AdminController.doDeletePlayer() with " + player.toString());

        playerSvc.remove(player);

  
        players = playerSvc.findAll();

        return "/admin/welcome";
    }

    public String executeSavePlayer() {
        LOG.info("Are you SURE that you have managed all your entities and set BOTH sides of your relationships?!?!? ");

        if (this.player.getId() != null) {
            LOG.info("Warning!  Warning!  We are about to UPDATE " + player.toString());
            // call the service layer to make the update
            playerSvc.update(player);
        } else {
            LOG.info("Warning!  Warning!  We are about to CREATE " + player.toString());
            // call the service layer to create
            playerSvc.create(player);
        }


        players = playerSvc.findAll();
        return "/admin/welcome";
    }


    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

}
