/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.web;

import edu.iit.sat.itmd4515.adamarjian.domain.Player;
import edu.iit.sat.itmd4515.adamarjian.domain.VideoGame;
import edu.iit.sat.itmd4515.adamarjian.service.PlayerService;
import edu.iit.sat.itmd4515.adamarjian.service.VideoGameService;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author adama
 */
@Named
@RequestScoped
public class PlayerController extends AbstractController {

    private static final Logger LOG = Logger.getLogger(PlayerController.class.getName());

    private Player player;
     private VideoGame videoGame;


    @EJB private PlayerService playerSvc;
    
    @EJB private VideoGameService VideoGameSvc;

    @Inject
    private LoginController loginController;
   
    

    @Override
    @PostConstruct
    protected void postConstruct() {
        super.postConstruct();
        LOG.info("Inside PlayerController postConstruct");
        player = playerSvc.findByUserName(loginController.getRemoteUser());
        videoGame = new VideoGame();
    }
    
    // helper method
    public String getGamePlayerSFormatted(VideoGame game){
        List<String> names = new ArrayList<>();
        for(Player player : game.getPlayers()){
            names.add(player.getName());
        }
        return String.join(",", names);
    }

    //place action methods here
    public String doViewGame(VideoGame game){
        this.videoGame = game;
        LOG.info("We are about to view the information for VideoGame " + videoGame.toString());
        return "/player/viewGame";
    }
    
    // TODO
    public String doEditGame(VideoGame game){
         this.videoGame = game;
        LOG.info("We are about to edit the information for VideoGame " + videoGame.toString());
        return "/player/editGame";
    }
    
    // TODO
    public String doDeleteGame(){
        LOG.info("We are about to delete the information for VideoGame " + videoGame.toString());
        return "/player/welcome";
    }

    // TODO
    public String doCreateGame(){
        LOG.info("We are about to create a new VideoGame" + videoGame.toString());
        return "/player/welcome";
    }

    public String executeSavePlayer() {
        LOG.info("Inside PlayerController executeSavePlayer to save"  + this.player.toString());
        return "/player/welcome";
    }
    public String executeSaveVideoGame() {
        LOG.info("Inside DiscJockeyController executeSaveRadioShow to save " + this.videoGame.toString());
        VideoGameSvc.update(videoGame);
        return "/player/welcome";
    }

    /**
     * Get the value of player
     *
     * @return the value of player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Set the value of player
     *
     * @param player new value of player
     */
    public void setPlayer(Player player) {
        this.player = player;
    }
    public VideoGame getVideoGame() {
        return videoGame;
    }
    public void setVideoGame(VideoGame videoGame) {
        this.videoGame = videoGame;
    }

}
