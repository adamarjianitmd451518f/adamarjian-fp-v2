/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.service;

import edu.iit.sat.itmd4515.adamarjian.domain.Bank;
import edu.iit.sat.itmd4515.adamarjian.domain.Player;
import edu.iit.sat.itmd4515.adamarjian.domain.VideoGame;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author EXO
 */
@Named
@Stateless
public class PlayerService extends AbstractService<Player> {

    public PlayerService() {
        super(Player.class);
    }
    
    
    @Override
    public void update(Player updatedPlayer) {

        
        Player oldPlayer = find(updatedPlayer.getId());
        Bank bank = getEntityManager().getReference(Bank.class, updatedPlayer.getBank().getId());
        // make sure we have all relationships set before updating
        updatedPlayer.setUser(oldPlayer.getUser());
        updatedPlayer.setVideogames(oldPlayer.getVideogames());
        updatedPlayer.setBank(bank);
        
        // finally update
        super.update(updatedPlayer);
    }

    @Override
    public void remove(Player player) {
        
        // dj starts as an unmanaged parameter!
        //
        // these both accomplish the same thing, turning dj
        // into a managed entity so it can be passed to em.remove
        //dj = getEntityManager().getReference(DiscJockey.class, dj.getId());
        //getEntityManager().remove(getEntityManager().merge(dj));
        //super.remove(dj);
        
        player = getEntityManager().getReference(Player.class, player.getId());
        player.removeBank();
        
        for(VideoGame game : player.getVideogames() ){
            game.getPlayers().remove(player);
        }
        player.setVideogames(null);
        getEntityManager().remove(player);
    }

    @Override
    public List<Player> findAll() {
        return getEntityManager().createNamedQuery("Player.findAll", Player.class).getResultList();
    }

    public Player findByUserName(String username) {
        return getEntityManager().createNamedQuery("Player.findByUsername", Player.class).setParameter("username", username).getSingleResult();
    }

}
