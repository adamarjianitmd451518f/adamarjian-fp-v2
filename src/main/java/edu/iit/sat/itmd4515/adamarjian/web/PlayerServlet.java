/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.web;

import edu.iit.sat.itmd4515.adamarjian.domain.Bank;
import edu.iit.sat.itmd4515.adamarjian.domain.Player;
import edu.iit.sat.itmd4515.adamarjian.domain.VideoGame;
import edu.iit.sat.itmd4515.adamarjian.service.BankService;
import edu.iit.sat.itmd4515.adamarjian.service.PlayerService;
import edu.iit.sat.itmd4515.adamarjian.service.VideoGameService;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

/**
 *
 * @author EXO
 */
@WebServlet(name = "PlayerServlet", urlPatterns = {"/player"})
public class PlayerServlet extends HttpServlet {


    @Resource
    Validator validator;

    @EJB
    PlayerService playerSvc;

    @EJB
    VideoGameService vgameSvc;

    private static final Logger LOG = Logger.getLogger(Player.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PlayerServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PlayerServlet at " + request.getContextPath() + "</h1>");
            out.println("<h2>Welcome" + request.getRemoteUser() + "</h2>");
            out.println("<a href=\"" + request.getContextPath() + "/logout\">Logout</a>");

            if (request.isUserInRole("PLAYER_ROLE")) {
                Player player = playerSvc.findByUserName(request.getRemoteUser());
                out.println("<h2>" + player.getUser().getUserName() + ": " + player.getName() + "</h2>");
                out.println("<h2>Birthday: " + player.getBirthday() + "</h2>");
                out.println("<h2>Email: " + player.getEmail() + "</h2>");
                out.println("<h2>Bank: " + player.getBank().getName() + "</h2>");
                out.println("<h2>Bank: </h2>");
                out.println("<ul>");
                for (VideoGame vgame : player.getVideogames()) {
                    out.println("<li>" + vgame.getName() + "</li>");
                }
                out.println("</ul>");
            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOG.info("Inside the doGet method");
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOG.info("Inside the doPost method");
        LOG.info("You passed a parameter called name with a value of " + request.getParameter("name"));
        LOG.info("You passed a parameter called email with a value of " + request.getParameter("email"));
        LOG.info("You passed a parameter called dateHired with a value of " + request.getParameter("dateHired"));

        String nameParam = request.getParameter("name");
        String emailParam = request.getParameter("email");
        String birthdayParam = request.getParameter("birthday");

        Date parsedDate = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        if (!birthdayParam.isEmpty()) {
            try {
                parsedDate = format.parse(birthdayParam);
            } catch (ParseException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }

        LOG.info("The length of |"
                + nameParam
                + "| is "
                + nameParam.length()
                + " and isEmpty returns "
                + nameParam.isEmpty()
        );

        Player player = new Player(nameParam, emailParam, parsedDate);
        LOG.info(player.toString());

        Set<ConstraintViolation<Player>> constraintViolations = validator.validate(player);
        if (constraintViolations.size() > 0) {
            LOG.info("We have an issue Failed validation");
            for (ConstraintViolation<Player> bad : constraintViolations) {
                LOG.info(bad.getPropertyPath() + " " + bad.getMessage());
            }

            request.setAttribute("player", player);
            if (player.getBirthday()!= null) {
                request.setAttribute("birthday", format.format(player.getBirthday()));
            }
            request.setAttribute("errors", constraintViolations);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/newplayerform.jsp");
            dispatcher.forward(request, response);

        } else {
            LOG.info("No problems with user input.  Proceed.");
            request.setAttribute("player", player);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/confirm.jsp");
            dispatcher.forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
