/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;


import edu.iit.sat.itmd4515.adamarjian.domain.Player;
import edu.iit.sat.itmd4515.adamarjian.domain.VideoGame;
import java.util.GregorianCalendar;
import org.junit.Test;

/**
 *
 * @author EXO
 * The users of these video games will be players and parents
 */
public class VideoGameTest extends AbstractJPATest {

    @Test
    public void persistNewVideoGame() {
        

        VideoGame vg = new VideoGame
        ("Blast Wars", new GregorianCalendar(2018, 01, 01).getTime());
        VideoGame vg2 = new VideoGame
        ("Blast Warsaaaaaaaaaaaaaaaa", new GregorianCalendar(2018, 01, 01).getTime());
        VideoGame vg3 = new VideoGame
        ("Bionic Commando", new GregorianCalendar(2018, 01, 01).getTime());

        tx.begin();
        em.persist(vg);
        em.persist(vg2);
        em.persist(vg3);
        tx.commit();

    }
    @Test
    public void persistNewPlayer() {
        Player player = new Player("Waad Nadder","email" ,new GregorianCalendar(2018, 01, 01).getTime());

        tx.begin();
        em.persist(player);
        tx.commit();

    }

}
