/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;

import edu.iit.sat.itmd4515.adamarjian.domain.Player;
import edu.iit.sat.itmd4515.adamarjian.domain.VideoGame;
import java.util.GregorianCalendar;
import org.junit.Test;

/**
 *
 * @author EXO
 */
public class PlayerTest extends AbstractJPATest {

    @Test
    public void persistNewPlayer() {
        Player player = new Player("Waad Nadder","waad@waad.com" ,new GregorianCalendar(2018, 01, 01).getTime());

        tx.begin();
        em.persist(player);
        tx.commit();

    }
}
